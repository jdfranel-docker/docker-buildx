FROM docker
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
ENV TARGETPLATFORM="linux/amd64,linux/arm,linux/arm64"
RUN docker buildx version
RUN docker buildx install